const url_base = "https://randomuser.me/api";

const resultEl = document.getElementById("result");
const filterEl = document.getElementById("filter");
const listItems = [];

const getData = async () => {
    const res = await fetch(url_base + "?results=50");
    const { results } = await res.json();
    resultEl.innerHTML = "";

    results.forEach((user) => {
        const li = document.createElement("li");
        listItems.push(li);
        li.innerHTML = `
        <img src="${user.picture.large}" alt="${user.name.first}">
        <div class="user-info">
            <h4>${user.name.first} ${user.name.last}</h4>
            <p>${user.location.city}, ${user.location.country}</p>
        </div>
        `;

        resultEl.appendChild(li);
    });
};

const filterData = (searchTerm) => {
    listItems.forEach((item) => {
        if (item.innerText.toLowerCase().includes(searchTerm.toLowerCase())) {
            item.classList.remove("hide");
        } else {
            item.classList.add("hide");
        }
    });
};

filterEl.addEventListener("input", (e) => filterData(e.target.value));

getData();
