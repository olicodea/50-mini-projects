const header = document.getElementById("header");
const title = document.getElementById("title");
const excerpt = document.getElementById("excerpt");
const profileImg = document.getElementById("profile_img");
const nameEl = document.getElementById("name");
const date = document.getElementById("date");

const animatedBgs = document.querySelectorAll(".animated-bg");
const animatedBgTexts = document.querySelectorAll(".animated-bg-text");

setTimeout(getData, 2000);

function getData() {
    header.innerHTML = `
    <img src="https://plus.unsplash.com/premium_photo-1674726322494-f972e5486bc4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=627&q=80" alt="premium photo"/>
    `;
    title.innerHTML = `Lorem ipsum dolor sit amer`;
    excerpt.innerHTML = `Lorem ipsum dolor sit amet consectetur, adipisicing elit.
    Vel amet accusantium sapiente facere quibusdam, consequuntur
    eaque ullam adipisci`;
    profileImg.innerHTML = `<img src="https://randomuser.me/api/portraits/men/45.jpg" alt="profile"/>
    `;
    nameEl.innerHTML = `John Doe`;
    date.innerHTML = `Oct 08, 2023`;

    animatedBgs.forEach((bg) => bg.classList.remove("animated-bg"));
    animatedBgTexts.forEach((bgText) =>
        bgText.classList.remove("animated-bg-text")
    );
}
