const addBtn = document.getElementById("add");

addBtn.addEventListener("click", () => addNewNote());

const notes = JSON.parse(localStorage.getItem("notes"));

if (notes) {
    notes.forEach((note) => addNewNote(note));
}

function addNewNote(text = "") {
    const noteEl = document.createElement("div");
    noteEl.classList.add("note");
    noteEl.innerHTML = `
    <div class="tools">
        <button class="edit"><i class="fa-solid fa-edit"></i></button>
        <button class="delete">
            <i class="fa-solid fa-trash-alt"></i>
        </button>
    </div>
    <div class="main ${text ? "" : "hidden"}"></div>
    <textarea class="${text ? "hidden" : ""}"></textarea>
    `;

    const editBtn = noteEl.querySelector(".edit");
    const deleteBtn = noteEl.querySelector(".delete");
    const mainEl = noteEl.querySelector(".main");
    const textEl = noteEl.querySelector("textarea");

    textEl.value = text;
    mainEl.innerHTML = marked(text);

    deleteBtn.addEventListener("click", () => {
        noteEl.remove();
        updateLS();
    });

    editBtn.addEventListener("click", () => {
        mainEl.classList.toggle("hidden");
        textEl.classList.toggle("hidden");
    });

    textEl.addEventListener("input", (e) => {
        const { value } = e.target;
        mainEl.innerHTML = marked(value);
        updateLS();
    });

    document.body.appendChild(noteEl);
}

function updateLS() {
    const notesText = document.querySelectorAll("textarea");
    const notes = [];

    notesText.forEach((noteText) => {
        notes.push(noteText.value);
    });

    localStorage.setItem("notes", JSON.stringify(notes));
}
