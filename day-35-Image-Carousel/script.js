const imgs = document.getElementById("imgs");
const leftBtn = document.getElementById("left");
const rightBtn = document.getElementById("right");
const img = document.querySelectorAll("#imgs img");

let idx = 0;

let interval = setInterval(run, 2000);

function run() {
    idx++;
    changeImage();
}

function changeImage() {
    let lastIdx = img.length - 1;

    if (idx > lastIdx) {
        idx = 0;
    } else if (idx < 0) {
        idx = lastIdx;
    }
    imgs.style.transform = `translateX(${-idx * 300}px)`;
}

function resetInterval() {
    clearInterval(interval);
    interval = setInterval(run, 2000);
}

rightBtn.addEventListener("click", () => {
    idx++;
    changeImage();
    resetInterval();
});

leftBtn.addEventListener("click", () => {
    idx--;
    changeImage();
    resetInterval();
});