const testimonialsContainer = document.querySelector(".testimonial-container");
const testimonial = document.querySelector(".testimonial");
const userImage = document.querySelector(".user-image");
const username = document.querySelector(".username");
const role = document.querySelector(".role");

const testimonials = [
    {
        name: "Miyah Myles",
        position: "Marketing",
        photo: "https://randomuser.me/api/portraits/women/46.jpg",
        text: "I've worked with literally hundreds of HTML/CSS devs and I have to say the top spot goes to this guy. This guy is an amazing dev. He stresses on good, clean code and pays heed to the details. I love devs who respect each and every aspect of a throughly thought out design and do their best to put it in code. He goes over and beyond and transforms ART into PIXELS - without a glitch, every time.",
    },
    {
        name: "June Cha",
        position: "Marketing",
        photo: "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=707b9c33066bf8808c934c8ab394dff6",
        text: "I've worked with literally hundreds of HTML/CSS devs and I have to say the top spot goes to this guy. This guy is an amazing dev. He stresses on good, clean code and pays heed to the details. I love devs who respect each and every aspect of a throughly thought out design and do their best to put it in code. He goes over and beyond and transforms ART into PIXELS - without a glitch, every time.",
    },
    {
        name: "Alex Danvers",
        position: "Marketing",
        photo: "https://randomuser.me/api/portraits/women/42.jpg",
        text: "I've worked with literally hundreds of HTML/CSS devs and I have to say the top spot goes to this guy. This guy is an amazing dev. He stresses on good, clean code and pays heed to the details. I love devs who respect each and every aspect of a throughly thought out design and do their best to put it in code. He goes over and beyond and transforms ART into PIXELS - without a glitch, every time.",
    },
    {
        name: "Maggie Chad",
        position: "Marketing",
        photo: "https://randomuser.me/api/portraits/women/40.jpg",
        text: "I've worked with literally hundreds of HTML/CSS devs and I have to say the top spot goes to this guy. This guy is an amazing dev. He stresses on good, clean code and pays heed to the details. I love devs who respect each and every aspect of a throughly thought out design and do their best to put it in code. He goes over and beyond and transforms ART into PIXELS - without a glitch, every time.",
    },
    {
        name: "Kara Danvers",
        position: "Marketing",
        photo: "https://randomuser.me/api/portraits/women/48.jpg",
        text: "I've worked with literally hundreds of HTML/CSS devs and I have to say the top spot goes to this guy. This guy is an amazing dev. He stresses on good, clean code and pays heed to the details. I love devs who respect each and every aspect of a throughly thought out design and do their best to put it in code. He goes over and beyond and transforms ART into PIXELS - without a glitch, every time.",
    },
];

let idx = 1;

function updateTestimonials() {
    const { name, position, photo, text } = testimonials[idx];
    testimonial.innerHTML = text;
    userImage.src = photo;
    username.innerHTML = name;
    role.innerHTML = position;

    idx++;

    if (idx > testimonials.length - 1) {
        idx = 0;
    }
}

setInterval(updateTestimonials, 10000);
