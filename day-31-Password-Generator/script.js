const resultEl = document.getElementById("result");
const lengthEl = document.getElementById("length");
const upperEl = document.getElementById("uppercase");
const lowerEl = document.getElementById("lowercase");
const numberEl = document.getElementById("numbers");
const symbolEl = document.getElementById("symbols");

const generateBtn = document.getElementById("generate");
const clipboardBtn = document.getElementById("clipboard");

const randomFunc = {
    lower: getRandomLower,
    upper: getRandomUpper,
    number: getRandomNumber,
    symbol: getRandomSymbol,
};

const copyTextToClipboard = async (text) => {
    try {
        await navigator.clipboard.writeText(`${text}`);
        alert("Password copied to clipboard!");
    } catch (error) {
        console.log("Error al copiar: ", error);
    }
};

clipboardBtn.addEventListener("click", () => {
    const password = resultEl.innerText;

    if (!password) {
        return;
    }

    copyTextToClipboard(password);
});

generateBtn.addEventListener("click", () => {
    const length = +lengthEl.value;

    const hasLower = lowerEl.checked;
    const hasUpper = upperEl.checked;
    const hasNumber = numberEl.checked;
    const hasSymbol = symbolEl.checked;

    resultEl.innerText = generatePassword(
        hasLower,
        hasUpper,
        hasNumber,
        hasSymbol,
        length
    );
});

function generatePassword(lower, upper, number, symbol, length) {
    let generatedPassword = "";
    const typesCount = lower + upper + number + symbol;
    const typesArr = [{ lower }, { upper }, { number }, { symbol }].filter(
        (item) => Object.values(item)[0]
    );

    if (typesCount === 0) {
        return "";
    }

    for (let i = 0; i < length; i += typesCount) {
        typesArr.forEach((type) => {
            const funcName = Object.keys(type)[0];
            generatedPassword += randomFunc[funcName]();
        });
    }

    const finalPassword = generatedPassword.slice(0, length);

    return finalPassword;
}

function getRandom(cant, base) {
    return String.fromCharCode(Math.floor(Math.random() * cant) + base);
}

function getRandomLower() {
    return getRandom(26, 97);
}

function getRandomUpper() {
    return getRandom(26, 65);
}

function getRandomNumber() {
    return getRandom(10, 48);
}

function getRandomSymbol() {
    const symbols = "!@#$%^&*(){}[]=<>(,.";
    return symbols[Math.floor(Math.random() * symbols.length)];
}
