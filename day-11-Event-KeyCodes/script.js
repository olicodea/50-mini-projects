const insert = document.getElementById("insert");
const principalInput = document.getElementById("PrincipalInput");

window.addEventListener("keydown", (e) => {
    insert.innerHTML = `
    <div  id="key" class="key">
        ${e.key === " " ? "Space" : e.key}
        <small>event.key</small>
    </div>
    <div  id="keyCode" class="key">
        ${e.keyCode}
        <small>event.keyCode</small>
    </div>
    <div id="code" class="key">
        ${e.code}
        <small>event.code</small>
    </div>`;
});

principalInput.addEventListener("keydown", (e) => {
    e.preventDefault();
    document.querySelector("#key").textContent = e.key;
    document.querySelector("#keyCode").textContent = e.keyCode;
    document.querySelector("#code").textContent = e.code;
}
)